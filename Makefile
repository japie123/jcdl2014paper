TARGET    = paper
 
PDFLATEX  = /usr/texbin/pdflatex
BIBTEX    = /usr/texbin/bibtex
LATEX     = /usr/texbin/pdflatex
 
all:
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode $(TARGET).tex
	$(BIBTEX) $(TARGET).aux
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode $(TARGET).tex
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode $(TARGET).tex
	open $(TARGET).pdf
 
ps:
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode -output-format=dvi $(TARGET).tex
	$(BIBTEX) $(TARGET).aux
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode -output-format=dvi $(TARGET).tex
	$(PDFLATEX) -synctex=1 -interaction=nonstopmode -output-format=dvi $(TARGET).tex
 
	dvips -o $(TARGET).ps $(TARGET).dvi
 
